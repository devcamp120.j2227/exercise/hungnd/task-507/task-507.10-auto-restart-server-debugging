// Import thư viện expressjs tương đương import express from "express";
const express = require("express");

// Khởi tạo 1 app express
const app = express();
// khai báo cổng chạy project
const port = 8000;
//Callback function là 1 function đóng vài trò là tham số của 1 function khác, nó sẽ thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    let currentDay = new Date();
    res.json({
        message: `Xin chào các bạn, hôm nay là ngày ${currentDay.getDate()} tháng ${currentDay.getMonth() + 1} năm ${currentDay.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})
